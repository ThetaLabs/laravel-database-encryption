<?php

namespace Tests;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Tests\Fixtures\Models\TestModel;
use Orchestra\Testbench\TestCase;

class HasEncryptedAttributesTest extends TestCase
{
    /**
     *
     */
    protected function setUp() : void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../Fixtures/Migrations');
        $this->withFactories(__DIR__.'/../Fixtures/Factories');
        Artisan::call('db:seed', ['--class' => 'Tests\Fixtures\Seeds\TestDatabaseSeeder']);
    }

    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function isEncrypted($value)
    {
        try {
            decrypt($value);
        } catch (DecryptException $ex) {
            return false;
        }

        return true;
    }

    public function testTraitBoots()
    {
        $this->assertEquals([
            'ssn' => [
                'hash' => 'sha256',
                'bi' => 'ssn_bi',
            ],
            "encrypt_string" => [
                "hash" => "sha256",
                "bi" => "encrypt_string_bi",
            ],
            "encrypt_integer" => [
                "hash" => "sha1",
                "bi" => "encrypt_integer_bi",
            ],
            "encrypt_boolean" => [
                "hash" => "sha256",
                "bi" => "enc_bool_bi",
            ],
            "encrypt_float" => [
                "hash" => "sha256",
                "bi" => "encrypt_float_bi",
            ],
            "encrypt_date" => [
                "hash" => "sha256",
                "bi" => "encrypt_date_bi",
            ],
            "encrypt_another_boolean" => [
                "hash" => "sha256",
                "bi" => false,
            ]
        ], TestModel::$encrypted);
    }

    /**
     * Tests to ensure BI's are hidden
     */
    public function testHiddenBi()
    {
        $m = TestModel::inRandomOrder()->first();

        $excluded = array_filter(Arr::pluck(TestModel::$encrypted, 'bi'), function ($item) {
            return $item;
        });

        foreach ($excluded as $key) {
            $this->assertArrayNotHasKey($key, $m->toArray());
        }
    }

    public function testNormalAttribute()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();

        $this->assertFalse($this->isEncrypted($m->name), 'Non-encrypted attribute is encrypted');
    }

    public function testDecrypts()
    {
        /** @var TestModel $m */
        $m = TestModel::where('name', 'NOT LIKE', 'test%')->first();

        $original = $m->getOriginal('encrypt_string');
        $this->assertTrue($this->isEncrypted($original), 'Value is not encrypted');

        $value = $m->encrypted_string;
        $this->assertTrue(! $this->isEncrypted($value), 'Value is still encrypted');
    }

    public function testBlindIndex()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();

        $this->assertEquals(
            hash_hmac('sha256', $m->encrypt_string, env('APP_KEY')),
            $m->encrypt_string_bi
        );

        $this->assertEquals(
            hash_hmac('sha1', $m->encrypt_integer, env('APP_KEY')),
            $m->encrypt_integer_bi
        );
    }

    public function testEncrypt()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();

        $originalEncrypted = $m->getOriginal('encrypt_string');
        $originalDecrypted = $m->getAttribute('encrypt_string');

        $new = $originalDecrypted . '123';
        $m->encrypt_string = $new;

        $newEncrypted = Arr::get($m->getDirty(), 'encrypt_string');
        $this->assertNotEquals($newEncrypted, $originalEncrypted, 'Encrypted value did not change');

        $this->assertEquals($new, $m->getAttribute('encrypt_string'), 'Encrypted value does not match');
    }

    public function testScopedWhere()
    {
        /** @var \Illuminate\Database\Query\Builder $q */
        $q = TestModel::whereEncrypted('encrypt_string', 'uniqueEmail@unique.com');
        $m = $q->first();

        $this->assertEquals(1, $q->count(), 'Where did not find models');

        $this->assertEquals($m->name, 'uniqueName123');
        $this->assertEquals($m->encrypt_string, 'uniqueEmail@unique.com');
        $this->assertEquals($m->encrypt_integer, 100);
        $this->assertEquals($m->encrypt_boolean, true);
        $this->assertEquals($m->encrypt_another_boolean, false);
        $this->assertEquals($m->encrypt_float, 10.0001);
        $this->assertEquals($m->encrypt_date, '2043-12-23 23:59:59');
    }

    public function testScopedWhereExceptionNonEncrypted()
    {
        // Negative case
        $this->expectExceptionMessage('Attempted to search on non-encrypted field name.');
        TestModel::whereEncrypted('name', 'test');
    }

    public function testScopedWhereExceptionNoBi()
    {
        $this->expectExceptionMessage('Unable to find blind index for encrypt_another_boolean.');
        TestModel::whereEncrypted('encrypt_another_boolean', 'false');
    }

    public function testScopedOrWhere()
    {
        /** @var \Illuminate\Database\Query\Builder $q */
        $q = TestModel::where(1, 2)
            ->orWhereEncrypted('encrypt_string', 'uniqueEmail@unique.com');

        $m = $q->first();
        $this->assertEquals(1, $q->count());

        $this->assertEquals($m->name, 'uniqueName123');
        $this->assertEquals($m->encrypt_string, 'uniqueEmail@unique.com');
        $this->assertEquals($m->encrypt_integer, 100);
        $this->assertEquals($m->encrypt_boolean, true);
        $this->assertEquals($m->encrypt_another_boolean, false);
        $this->assertEquals($m->encrypt_float, 10.0001);
        $this->assertEquals($m->encrypt_date, '2043-12-23 23:59:59');
    }

    public function testExplodeToSearchables()
    {
        /** @var \Illuminate\Database\Query\Builder $q */
        $q = TestModel::whereEncrypted('ssn', '123-45-6789');
        /** @var TestModel $m */
        $m = $q->first();

        $this->assertGreaterThanOrEqual(1, $q->count());
        $this->assertEquals([
            '123',
            '45',
            '6789'
        ], $m->explodeSsnToSearchables($m->ssn));

        $q = TestModel::whereEncrypted('ssn', 'like', '6789');
        $this->assertGreaterThanOrEqual(1, $q->count());

        $q = TestModel::whereEncrypted('ssn', 'like', '123')
            ->whereEncrypted('ssn', 'like', '45');
        $this->assertEquals(1, $q->count());

        $m = $q->first();
        $this->assertEquals($m->name, 'uniqueName123');
        $this->assertEquals($m->ssn, '123-45-6789');
        $this->assertEquals($m->encrypt_string, 'uniqueEmail@unique.com');
        $this->assertEquals($m->encrypt_integer, 100);
        $this->assertEquals($m->encrypt_boolean, true);
        $this->assertEquals($m->encrypt_another_boolean, false);
        $this->assertEquals($m->encrypt_float, 10.0001);
        $this->assertEquals($m->encrypt_date, '2043-12-23 23:59:59');
    }

    public function testToArray()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();

        $keys = [
            'id',
            'name',
            'ssn',
            'encrypt_string',
            'encrypt_integer',
            'encrypt_boolean',
            'encrypt_another_boolean',
            'encrypt_float',
            'encrypt_date',
            'created_at',
            'updated_at',
        ];

        $actual = array_keys($m->toArray());

        $this->assertEquals($keys, $actual);
    }

    public function testHiddenAttributesStillWork()
    {
        $m = TestModel::inRandomOrder()->first();

        $data = $m->toArray();

        $this->assertArrayNotHasKey('you_cant_see_me', $data);
    }

    public function testDecryptOnJson()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();
        $json = $m->toJson();

        $this->assertJson($json);

        $array = json_decode($json, true);
        $this->assertIsArray($array);
        $this->assertArrayHasKey('ssn', $array);
        $this->assertArrayHasKey('encrypt_string', $array);
        $this->assertArrayNotHasKey('ssn_bi', $array);

        $this->assertFalse($this->isEncrypted($array['ssn']));
        $this->assertFalse($this->isEncrypted($array['encrypt_string']));
    }

    public function testCachingDecryptedAttributes()
    {
        /** @var TestModel $m */
        $m = TestModel::inRandomOrder()->first();
        $beforeSsn = $m->getDecryptedHashedAttribute('ssn');

        $start = microtime(true);
        $m->ssn;
        $end = microtime(true);
        $preCache = $end - $start;

        $start = microtime(true);
        $m->ssn;
        $end = microtime(true);
        $postCache = $end - $start;

        $afterSsn = $m->getDecryptedHashedAttribute('ssn');

        $this->assertNull($beforeSsn);
        $this->assertEquals($m->ssn, $afterSsn);
        $this->assertLessThanOrEqual($preCache, $postCache);
    }
}
