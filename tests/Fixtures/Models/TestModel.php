<?php

namespace Tests\Fixtures\Models;

use Illuminate\Database\Eloquent\Model;
use ThetaLabs\DbEncryption\HasEncryptedAttributes;

class TestModel extends Model
{
    use HasEncryptedAttributes;

    protected $table = 'test_tables';

    protected $connection = 'testbench';

    protected $guarded = [];

    protected $fillable = [
        'name',
        'ssn',
        'encrypt_string',
        'encrypt_integer',
        'encrypt_boolean',
        'encrypt_another_boolean',
        'encrypt_float',
        'encrypt_date',
        'you_cant_see_me'
    ];

    public static $encrypted = [
        'ssn' => 'bi',
        'encrypt_string' => 'bi',
        'encrypt_integer' => 'bi,hash:sha1',
        'encrypt_boolean' => 'bi:enc_bool_bi',
        'encrypt_another_boolean',
        'encrypt_float' => 'bi',
        'encrypt_date' => 'bi',
    ];

    protected $hidden = [
        'you_cant_see_me'
    ];

    public function explodeSsnToSearchables($ssn)
    {
        return explode('-', $ssn);
    }
}
