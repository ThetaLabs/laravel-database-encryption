<?php

namespace ThetaLabs\DbEncryption;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use InvalidArgumentException;

/** @extends \Illuminate\Database\Eloquent\Model */
trait HasEncryptedAttributes
{
    /**
     * Indicate which model attributes are to be encrypted
     *
     * @var array
     */

//    protected static $encrypted = [
//        'ssn' => 'bi:ssn_bi,hash:sha1',
//        'name',
//    ];

    /*
     * Decrypted attribute cache
     */
    protected $decrypted = [];

    /**
     * Boots the trait and makes options easier to access
     */
    protected static function bootHasEncryptedAttributes()
    {
        foreach (self::$encrypted as $field => $line) {
            if (is_array($line))
                continue;

            list('key' => $k, 'value' => $v) = self::explodeOptions($field);

            if (is_numeric($field))
                unset(self::$encrypted[$field]);

            self::$encrypted[$k] = $v;
        }
    }

    /**
     * Get the options for a field
     */
    protected function getEncryptedOptions($field)
    {
        return static::$encrypted[$field];
    }

    /**
     * Calculate the options for a field
     *
     * @param $field
     * @return array
     */
    protected static function explodeOptions($field)
    {
        $raw = self::$encrypted[$field];

        $lines = [];
        if (is_numeric($field)) {
            $field = $raw;
        } else {
            $lines = explode(',', $raw);
        }

        $options = ['hash' => 'sha256', 'bi' => false];
        foreach ($lines as $line) {
            $e = explode(':', $line);
            $k = array_shift($e);
            $v = array_shift($e);

            if ($k === 'bi' && is_null($v)) {
                $v = $field.'_bi';
            }

            $options[$k] = $v;
        }

        return ['key' => $field, 'value' => $options];
    }

    protected function isEncryptedAttribute($key)
    {
        return array_key_exists($key, static::$encrypted) ||
             in_array($key, static::$encrypted);
    }

    protected function isDecryptedAttribute($key)
    {
        return array_key_exists($key, $this->decrypted);
    }

    /**
     * @param $key
     * @param $value
     * @return string
     */
    protected function setEncryptedAttribute($key, $value)
    {
        // first check if field has a mutator and use it
        if ($this->hasSetMutator($key)) {
            $value = $this->setMutatedAttributeValue($key, $value);
        }

        $options = $this->getEncryptedOptions($key);

        if ($bi = $options['bi']) {
            $hash = $options['hash'];
            $searchables = is_null($value) ?
                $value : implode(',', $this->getHashedSearchables($hash, $key, $value));

            $this->{$bi} = $searchables;
        }

        $encrypted = is_null($value) ? $value : Crypt::encrypt($value);
        $this->decrypted[$key] = $value;
        $this->attributes[$key] = $encrypted;

        return $encrypted;
    }

    protected function decryptIfEncrypted($key)
    {
        if (! $this->isEncryptedAttribute($key))
            return parent::getAttributeFromArray($key);

        if (! $this->isDecryptedAttribute($key)) {
            $value = parent::getAttributeFromArray($key);
            $this->decrypted[$key] = ! is_null($value) ?
                decrypt($value) : null;
        }

        return $this->decrypted[$key];
    }

    public function getDecryptedHashedAttribute($field)
    {
        if (array_key_exists($field, $this->decrypted))
            return $this->decrypted[$field];

        return null;
    }

    protected function getHashedSearchable($alg, $field, $value)
    {
        return current($this->getHashedSearchables($alg, $field, $value, true));
    }

    protected function getHashedSearchables($alg, $field, $value, $originalOnly = false)
    {
        $method = 'explode'.Str::studly($field).'ToSearchables';

        $searchables = [$value];
        if (! $originalOnly && method_exists($this, $method))
            $searchables = array_merge($searchables, $this->{$method}($value));

        foreach ($searchables as &$searchable) {
            if (is_null($searchable))
                continue;

            $searchable = hash_hmac($alg, (string) $searchable, env('APP_KEY'));
        }

        return $searchables;
    }

    public function scopeWhereEncrypted($query, $field, $operator, $value = null)
    {
        if (func_num_args() <= 3) {
            $value = $operator;
            $operator = '=';
        }

        $this->doWhereEncrypted('where', $query, $field, $operator, $value);
    }

    public function scopeOrWhereEncrypted($query, $field, $operator, $value = null)
    {
        if (func_num_args() <= 3) {
            $value = $operator;
            $operator = '=';
        }

        $this->doWhereEncrypted('orWhere', $query, $field, $operator, $value);
    }

    protected function doWhereEncrypted($type, $query, $field, $operator, $value)
    {
        /** @var \Illuminate\Database\Eloquent\Builder $query */
        if (! $this->isEncryptedAttribute($field))
            throw new InvalidArgumentException("Attempted to search on non-encrypted field ${field}.");

        list('hash' => $alg, 'bi' => $searchColumn) = $this->getEncryptedOptions($field);

        if (! $searchColumn)
            throw new InvalidArgumentException("Unable to find blind index for ${field}.");

        if (strtolower($operator) != 'like')
            return $query->{$type}(
                $searchColumn, 'like', '%'.$this->getHashedSearchable($alg, $field, $value).'%'
            );

        $searchables = $this->getHashedSearchables($alg, $field, $value);
        return $query->{$type}(function($q) use ($field, $searchables, $searchColumn, $type, $operator) {
            foreach ($searchables as $searchable) {
                $q->orWhere($searchColumn, 'like', '%'.$searchable.'%');
            }
        });
//        return $query->{$type}($searchColumn, $this->getEncryptedHash($alg, $field, $value));
    }

    //region Eloquent Model overrides!

    /**
     * @param $key
     * @return mixed
     */
    protected function getAttributeFromArray($key)
    {
        return $this->decryptIfEncrypted($key);
    }

    /**
     * @return array
     */
    protected function getArrayableAttributes()
    {
        $exclude = array_filter(Arr::pluck(static::$encrypted, 'bi'), function ($item) {
            return $item;
        });

        $this->makeHidden($exclude);

        $array = parent::getArrayableAttributes();
        foreach ($array as $key => $value) {
            $array[$key] = $this->decryptIfEncrypted($key);
        }

        return $array;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        if ($this->isEncryptedAttribute($key))
            return $this->setEncryptedAttribute($key, $value);

        return parent::setAttribute($key, $value);
    }


    /**
     * List all encrypted columns so we can do some magic after
     *
     * @return array
     */
    public function encryptedColumns()
    {
        return empty(self::$encrypted) ? [] : self::$encrypted;
    }

    //endregion
}
